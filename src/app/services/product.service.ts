import { Injectable } from '@angular/core';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: Product[] = [
    {
      id: "GR1",
      name: "Green tea",
      price: 3.11,
      counter: 0,
      sale: false,
      freeProduct: 0,
    },

    {
      id: "SR1",
      name: "Strawberries",
      price: 5.00,
      counter: 0,
      sale: false,
      freeProduct: 0,
    },

    {
      id: "CF1",
      name: "Coffee",
      price: 11.23,
      counter: 0,
      sale: false,
      freeProduct: 0,
    }
  ];

  selectedProduct = [];
  freeProducts = [];

  constructor() { }

  addCart(product) {
    if (product.counter === 0) {
      product.counter++;
      this.selectedProduct.push(product);

    } else if (product.counter > 0) {
      for (var i = 0; i < this.selectedProduct.length; i++) {
        if (this.selectedProduct[i].id === product.id) {
          this.selectedProduct[i].counter++;
        } else {
        }
      }
    }
    console.log("El carrito ha quedado: ", this.selectedProduct);
    this.checkSale(product);
  }

  checkSale(product) {
    for (var i = 0; i < this.selectedProduct.length; i++) {
      //Oferta del Strawberry
      if (this.selectedProduct[i].id === "SR1"
        && this.selectedProduct[i].counter > 2
        && product.id === "SR1") {
        this.selectedProduct[i].price = 4.50;
        this.selectedProduct[i].sale = true;
      }
      //Oferta del Coffe
      else if (this.selectedProduct[i].id === "CF1"
        && this.selectedProduct[i].counter > 2
        && product.id === "CF1") {
        this.selectedProduct[i].price = (11.23 * 2 / 3).toFixed(2);
        this.selectedProduct[i].sale = true;
      }
      //Oferta del Green Tea
      else if (this.selectedProduct[i].id === "GR1"
        && product.id === "GR1") {
        this.selectedProduct[i].price = (3.11 / 2);
        this.selectedProduct[i].sale = true;
        this.selectedProduct[i].counter++;
      }
    }
  }

  deleteCart(product) {
    for (var i = 0; i < this.selectedProduct.length; i++) {
      if (this.selectedProduct[i].id === product.id && product.counter > 0) {
        this.selectedProduct[i].counter--;

      } else if (product.counter === 1) {
        // this.selectedProduct[i].splice(i, 1);
      }
    }
    console.log("2. El producto eliminado es:", product);
    console.log("3. El carrito ha quedado así: ", this.selectedProduct);
    this.deleteSale(product);
  }

  deleteSale(product) {
    for (var i = 0; i < this.selectedProduct.length; i++) {
      //Quitar oferta de Strawberry
      if (this.selectedProduct[i].id === "SR1"
        && this.selectedProduct[i].counter <= 2
        && product.id === "SR1") {
        this.selectedProduct[i].price = 5;
        this.selectedProduct[i].sale = false;
      }
      //Quitar oferta de Coffe
      else if (this.selectedProduct[i].id === "CF1"
        && this.selectedProduct[i].counter <= 2
        && product.id === "CF1") {
        this.selectedProduct[i].price = 11.23;
        this.selectedProduct[i].sale = false;
      }
      //Quitar oferta de Green Tea
      else if (this.selectedProduct[i].id === "GR1"
        && product.id === "GR1"
        && this.selectedProduct[i].counter == 0) {
        this.selectedProduct[i].sale = false;
        this.selectedProduct[i].price = 3.11;
      }
      else if (this.selectedProduct[i].id === "GR1"
        && product.id === "GR1"
        && this.selectedProduct[i].counter > 0) {
        this.selectedProduct[i].counter--;
      }
    }
  }



  getCart() {
    return this.selectedProduct;
  }

  getProducts() {
    return this.products;
  }


  totalPrice() {
    var totalPriceCart = 0;
    for (var i = 0; i < this.selectedProduct.length; i++) {
      if (this.selectedProduct[i].id === "CF1" && this.selectedProduct[i].sale == true) {
      }
      totalPriceCart += this.selectedProduct[i].price * this.selectedProduct[i].counter;
    }
    return totalPriceCart;
  }

}