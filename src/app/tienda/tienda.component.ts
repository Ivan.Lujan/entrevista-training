import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/product.service';
import { Product } from '../models/product';

@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.scss']
})
export class TiendaComponent implements OnInit {
 
  products;
  
  constructor(
    private productsService: ProductsService,
    ) {   }

  ngOnInit() {
   this.products = this.productsService.getProducts();
  }

  addCart(product) {
    console.log("Añadiendo al carrito: ", product);
    this.productsService.addCart(product);
    
  }

  deleteCart(product){
    console.log("Quitando del carrito: ", product);
    this.productsService.deleteCart(product);
  }


}
