import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/product.service';
import { Product } from '../models/product';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {

  
  selected =[];
  totalPriceCart;
  productCounterCart;

  constructor(
    private productsService: ProductsService
  ) { }

  ngOnInit() {
    this.selected = this.productsService.getCart();
    this.totalPrice();
    
  }

  totalPrice(){
    this.totalPriceCart = this.productsService.totalPrice();

  }


    


}
