export class Product {
    id: string;
    name: string;
    price: number;
    counter: number;
    sale: boolean;
    freeProduct: number;
}